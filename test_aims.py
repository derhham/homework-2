from nose.tools import assert_equal
import aims
def test_positive():
    list = [3,2,5,7, 3]
    abss = aims.std(list)
    exp = 0.44721359549995793
    assert_equal(abss, exp)

def test_negative():
    list = [-3,-3,-5,-7 -4]
    abss = aims.std(list)
    exp =2.75
    assert_equal(abss, exp)

def test_float():
    list = [0.2, 0.4 ,0.5,7.8]
    abss = aims.std(list)
    exp =2.7874999999999996
    assert_equal(abss, exp)



def test_avg1():
    files = ['data/bert/audioresult-00215']
    abs = aims.avg_range(files)
    exp = 5.0
    assert_equal(abs, exp)

def test_avg2():
    files = ['data/bert/audioresult-00317']
    abs = aims.avg_range(files)
    exp = 6.0
    assert_equal(abs, exp)

def test_avg3():
    files = ['data/bert/audioresult-00384']
    abs = aims.avg_range(files)
    exp = 1.0
    assert_equal(abs, exp)

def test_avg4():
    files = ['data/bert/audioresult-00557']
    abs = aims.avg_range(files)
    exp = 2.0
    assert_equal(abs, exp)
